package eu.kudan.kudansamples;

import android.support.v4.view.GestureDetectorCompat;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;

import com.jme3.math.Vector3f;

import eu.kudan.kudan.ARActivity;
import eu.kudan.kudan.ARImageTrackable;
import eu.kudan.kudan.ARImageTrackableListener;
import eu.kudan.kudan.ARImageTracker;
import eu.kudan.kudan.ARLightMaterial;
import eu.kudan.kudan.ARMeshNode;
import eu.kudan.kudan.ARModelImporter;
import eu.kudan.kudan.ARModelNode;
import eu.kudan.kudan.ARTexture2D;
import eu.kudan.kudan.ARTexture3D;

public class ARCameraActivity extends ARActivity {

    private ARImageTrackable trackable;
    private GestureDetectorCompat gestureDetector;
    private ARModelNode arbiModelNode;
    private LinearLayout mTextLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_arcamera);
        this.gestureDetector = new GestureDetectorCompat(this, new MyGestureListener());
        mTextLayout = (LinearLayout) findViewById(R.id.text_layout);
    }

    public void setup() {
        addModelNode();
        addImageTrackable();
    }

    class MyGestureListener extends GestureDetector.SimpleOnGestureListener {
        MyGestureListener() {
        }

        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            if (ARCameraActivity.this.arbiModelNode != null) {
                ARCameraActivity.this.arbiModelNode.rotateByDegrees(distanceX, 0.0f, -1.0f, 0.0f);
            }
            return true;
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        this.gestureDetector.onTouchEvent(event);
        return super.onTouchEvent(event);
    }

    private void addImageTrackable() {

        // Initialise image trackable
        trackable = new ARImageTrackable("credit_card2");
        trackable.loadFromAsset("credit_card2.jpeg");

        // Get instance of image tracker manager
        ARImageTracker trackableManager = ARImageTracker.getInstance();

        // Add image trackable to image tracker manager
        trackableManager.addTrackable(trackable);

        // Add model node to image trackable
        trackable.getWorld().addChild(arbiModelNode);
        trackable.getWorld().getChildren().get(0).setVisible(true);
        trackable.addListener(new ARImageTrackableListener() {
            @Override
            public void didDetect(ARImageTrackable arImageTrackable) {
                mTextLayout.setVisibility(View.INVISIBLE);
            }

            @Override
            public void didTrack(ARImageTrackable arImageTrackable) {

            }

            @Override
            public void didLose(ARImageTrackable arImageTrackable) {
                mTextLayout.setVisibility(View.VISIBLE);
            }
        });
    }

    private void addModelNode() {
        setUpArbiModel();
//        // Import model
//        ARModelImporter modelImporter = new ARModelImporter();
//        modelImporter.loadFromAsset("ben.jet");
//        arbiModelNode = modelImporter.getNode();
//
//        // Load model texture
//        ARTexture2D texture2D = new ARTexture2D();
//        texture2D.loadFromAsset("bigBenTexture.png");
//
//        // Apply model texture to model texture material
//        ARLightMaterial material = new ARLightMaterial();
//        material.setTexture(texture2D);
//        material.setAmbient(0.8f, 0.8f, 0.8f);
//
//        // Apply texture material to models mesh nodes
//        for(ARMeshNode meshNode : modelImporter.getMeshNodes()){
//            meshNode.setMaterial(material);
//        }
//
//
//        arbiModelNode.rotateByDegrees(90,1,0,0);
//        arbiModelNode.scaleByUniform(0.25f);
//
//        // Add model node to image trackable
//        trackable.getWorld().addChild(arbiModelNode);
//        arbiModelNode.setVisible(true);

    }

    public void setUpArbiModel() {
        ARModelImporter importer = new ARModelImporter();
        importer.loadFromAsset("bloodhound.jet");
        this.arbiModelNode = importer.getNode();
        ARTexture2D texture2D = new ARTexture2D();
        texture2D.loadFromAsset("bloodhound.png");
        ARLightMaterial material = new ARLightMaterial();
        material.setTexture(texture2D);
        material.setDiffuse(0.2f, 0.2f, 0.2f);
        material.setAmbient(0.8f, 0.8f, 0.8f);
        material.setSpecular(0.3f, 0.3f, 0.3f);
        material.setShininess(20.0f);
        material.setReflectivity(0.15f);
        Vector3f lightDirection = new Vector3f(0.0f, -1.0f, 0.0f);
        material.setCubeTexture(new ARTexture3D("chrome_b.png", "chrome_f.png", "chrome_u.png", "chrome_d.png", "chrome_r.png", "chrome_l.png"));
        for (ARMeshNode meshNode : importer.getMeshNodes()) {
            meshNode.setMaterial(material);
            meshNode.setLightDirection(lightDirection);
        }

        this.arbiModelNode.rotateByDegrees(90,1,0,0);
        this.arbiModelNode.scaleByUniform(20.0f);
    }
}
